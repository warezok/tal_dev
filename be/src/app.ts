import express from 'express';
import { createServer, Server } from 'http';
import { json } from 'body-parser';

import { transports, format } from 'winston';
import { logger, errorLogger } from 'express-winston';
import cors from 'cors';
import debug from 'debug';
import { CommonRoutesConfig } from './common/common.routes.config';
import { UsersRoutes } from './users/users.routes.config';

export const app: express.Application = express();
export const server: Server = createServer(app);
const port = process.env.BE_PORT || 8000;
const routes: Array<CommonRoutesConfig> = [];
const debugLog: debug.IDebugger = debug('app');

// here we are adding middleware to parse all incoming requests as JSON
app.use(json());

// here we are adding middleware to allow cross-origin requests
app.use(cors());

// here we are configuring the expressWinston logging middleware,
// which will automatically log all HTTP requests handled by Express.js
app.use(
  logger({
    transports: [new transports.Console()],
    format: format.combine(format.colorize(), format.json()),
  }),
);

// here we are adding the UserRoutes to our array,
// after sending the Express.js application object to have the routes added to our app!
routes.push(new UsersRoutes(app));

// here we are configuring the expressWinston error-logging middleware,
// which doesn't *handle* errors per se, but does *log* them
app.use(
  errorLogger({
    transports: [new transports.Console()],
    format: format.combine(format.colorize(), format.json()),
  }),
);

// this is a simple route to make sure everything is working properly
app.get('/', (req: express.Request, res: express.Response) => {
  res.status(200).send('Server up and running!');
});

server.listen(port, () => {
  debugLog(`Server running at http://localhost:${port}`);
  routes.forEach((route: CommonRoutesConfig) => {
    debugLog(`Routes configured for ${route.getName()}`);
  });
});
