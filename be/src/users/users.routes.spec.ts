import request from 'supertest';
import { app } from '../app';
import data from '../data/MOCK_DATA.json';

describe('Test the users path', () => {
  test('It should give the users list', async () => {
    const response = await request(app).get('/users');
    expect(response.status).toBe(200);
    expect(response.body.length).toBe(data.length);
    expect(response.body[0]).toHaveProperty('firstName', data[0].first_name);
  });

  test('It should give the single user info', async () => {
    const response = await request(app).get('/users/' + data[0].id);
    expect(response.status).toBe(200);
    expect(response.body).toStrictEqual(data[0]);
  });
});
