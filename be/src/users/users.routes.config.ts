import express from 'express';
import { CommonRoutesConfig } from '../common/common.routes.config';
import data from '../data/MOCK_DATA.json';

/**
 * Concrete routes class for fetching users
 */
export class UsersRoutes extends CommonRoutesConfig {
  constructor(app: express.Application) {
    super(app, 'UsersRoutes');
  }

  configureRoutes() {
    //Adding endpoint for users list
    this.app.route('/users').get((req: express.Request, res: express.Response) => {
      res.status(200).send(
        (data || {}).map((record) => ({
          firstName: record.first_name,
          id: record.id,
          lastName: record.last_name,
        })),
      );
    });

    //Adding endpoint for single user details
    this.app.route('/users/:userId').get((req: express.Request, res: express.Response) => {
      res.status(200).send((data || {}).find((record) => String(record.id) === req.params.userId));
    });

    return this.app;
  }
}
