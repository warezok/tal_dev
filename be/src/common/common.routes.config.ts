import express from 'express';

/**
 * Abstract class for adding new routes
 */
export abstract class CommonRoutesConfig {
  app: express.Application;
  name: string;

  protected constructor(app: express.Application, name: string) {
    this.app = app;
    this.name = name;
    this.configureRoutes();
  }
  getName() {
    return this.name;
  }

  /**
   * Implement this method with all necessary endpoints there
   * return this.app at the end
   */
  abstract configureRoutes(): express.Application;
}
