import request from 'supertest';
import { app, server } from './app';

describe('Test the root path', () => {
  test('It should response the GET method', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
  });

  afterAll(async () => server.close());
});
