{
  "extends": [
    "plugin:prettier/recommended",
    "airbnb"
  ],
  "plugins": [
    "@typescript-eslint",
    "jest",
    "typescript-sort-keys"
  ],
  "parser": "@typescript-eslint/parser",
  "env": {
    "browser": true,
    "jest/globals": true
  },
  "rules": {
    "sort-keys": ["error", "asc", {"caseSensitive": true, "natural": true, "minKeys": 3}],
    "typescript-sort-keys/interface": "error",
    "typescript-sort-keys/string-enum": "error",
    "arrow-parens": "off",
    "arrow-body-style": ["error", "as-needed"],
    "comma-dangle": [
      "error",
      "only-multiline"
    ],
    "comma-style": ["error", "last"],
    "consistent-return": "off",
    "function-paren-newline": "off",
    "implicit-arrow-linebreak": "off",
    "import/prefer-default-export": "off",
    "prefer-object-spread": "off",
    "no-param-reassign": "off",
    "prefer-template": "off",
    "import/no-extraneous-dependencies": "off",
    "jsx-a11y/click-events-have-key-events": "off",
    "jsx-a11y/label-has-associated-control": "off",
    "jsx-a11y/label-has-for": "off",
    "jsx-a11y/no-noninteractive-element-interactions": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    "max-len": ["error", 140, 2],
    "no-case-declarations": "off",
    "no-mixed-operators": "warn",
    "no-plusplus": "off",

    // Overrides Prettier indent validator and sometimes produce unexpected warns
    // Disabled => Prettier indentation validation remained
    "indent": "off",

    "no-underscore-dangle": [
      "warn",
      {
        "allow": ["__REDUX_DEVTOOLS_EXTENSION__"]
      }
    ],
    "no-use-before-define": [
      "error",
      {
        "functions": false,
        "classes": false,
        "variables": true
      }
    ],
    "no-useless-constructor": "warn",
    "object-curly-newline": [
      "error",
      {
        "consistent": true,
        "multiline": true
      }
    ],
    "one-var": "off",
    "operator-linebreak": "off",
    "padded-blocks": "off",
    "prefer-destructuring": "warn",
    "spaced-comment": "off",
    "import/no-unresolved": 0,
    "import/extensions": "off",
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": ["error", {
      "vars": "all",
      "args": "after-used",
      "ignoreRestSiblings": false
    }],
    "lines-between-class-members": "off"
  }
}
