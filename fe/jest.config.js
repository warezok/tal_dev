module.exports = {
  // Beautify tests output
  displayName: {
    color: 'yellow',
    name: 'globaledit',
  },

  maxConcurrency: 20,
  moduleDirectories: ['node_modules', 'bower_components'],
  moduleNameMapper: {
    '\\.(css|less|scss|sss|styl)$': '<rootDir>/node_modules/jest-css-modules',
  },
  roots: ['<rootDir>/src'],
  setupFilesAfterEnv: ['jest-expect-message', '<rootDir>/jest.setup.ts'],
  testMatch: ['**/__tests__/**/*.+(ts|tsx|js)', '**/?(*.)+(spec|test).+(ts|tsx|js)'],
  transform: {
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2|svg)$': 'jest-transform-stub',
    '^.+\\.(js|jsx|ts|tsx)$': 'ts-jest',
  },
};
