import axios from 'axios';
import axiosRetry from 'axios-retry';
import { baseURL } from './utilities/env';

export const apiClient = axios.create({
  baseURL,
});

axiosRetry(apiClient, { retries: 2 });
