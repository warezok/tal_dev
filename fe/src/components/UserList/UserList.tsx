/* eslint react/jsx-props-no-spreading:off */
import React, { FC, useEffect } from 'react';
import './UserList.scss';
import { useDispatch, useSelector } from 'react-redux';
import { DataGrid, GridColDef, GridRowSelectedParams } from '@material-ui/data-grid';
import { selectDetails, selectUsers } from '../../selectors/UserDataSelectors';
import logo from '../../logo.svg';
import { closeUser, getUser, getUsers } from '../../actions/UserDataActions';
import { User } from '../User/User';
import { Drawer } from '../Drawer/Drawer';

export const UserList: FC = () => {
  const users = useSelector(selectUsers);
  const details = useSelector(selectDetails);
  const dispatch = useDispatch();
  const columns: GridColDef[] = [
    { field: 'firstName', headerName: 'First Name', width: 150 },
    { field: 'lastName', headerName: 'Last Name', width: 150 },
  ];
  useEffect(() => {
    dispatch(getUsers());
  }, []);

  const onRowSelected = (param: GridRowSelectedParams) => {
    dispatch(getUser(String(param.data.id)));
  };

  const onExit = () => {
    dispatch(closeUser());
  };
  const nodeRef = React.useRef(null);
  return (
    <>
      <div className="user_list">
        {(!users || users.length === 0) && <img ref={nodeRef} alt="logo" className="app-logo" src={logo} />}
        {users?.length > 0 && <DataGrid columns={columns} onRowSelected={onRowSelected} rows={users} />}
      </div>
      <Drawer isOpen={!!details} onClickOverlay={onExit} onExit={onExit}>
        <User {...details} />
      </Drawer>
    </>
  );
};
