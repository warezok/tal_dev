import React, { FC } from 'react';
import { DetailedUser } from '../../types/UserDataState';
import './User.scss';

export const User: FC<Partial<DetailedUser>> = (props: Partial<DetailedUser>) => {
  // eslint-disable-next-line camelcase
  const { country, email, first_name, gender, id, job_title, last_name } = props;
  const data = [
    { name: 'First Name', data: first_name },
    { name: 'Last Name', data: last_name },
    { name: 'Email', data: email },
    { name: 'Gender', data: gender },
    { name: 'Job Title', data: job_title },
    { name: 'Country', data: country },
  ];
  return (
    <div className="user">
      {data.map((record) => (
        <div key={record.name + id} className="user__row">
          <div className="user_cell -bold">{record.name}</div>
          <div className="user_cell">{record.data}</div>
        </div>
      ))}
    </div>
  );
};
