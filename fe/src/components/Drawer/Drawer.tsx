import React from 'react';
import './Drawer.scss';
import { CSSTransition } from 'react-transition-group';
import classnames from 'classnames';
import { defaultDrawerProps, DrawerProps } from './types/DrawerProps';

export class Drawer extends React.Component<DrawerProps> {
  static defaultProps = defaultDrawerProps();

  render() {
    const { children, isOpen, drawerId, onClickOverlay, onExit, layer } = this.props;
    return (
      <CSSTransition
        key={drawerId}
        classNames="slideInFromRight"
        in={isOpen}
        mountOnEnter={true}
        onExited={onExit}
        timeout={350}
        unmountOnExit={true}
      >
        <>
          <div className={classnames('drawer', `drawer_${drawerId}`, `drawer_layer-${layer}`)} id={drawerId}>
            {children}
          </div>
          <div className="modal-bkg" onClick={onClickOverlay} />
        </>
      </CSSTransition>
    );
  }
}
