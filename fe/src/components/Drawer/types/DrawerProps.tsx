import React, { ReactNode } from 'react';

export interface DrawerProps {
  children: ReactNode;
  drawerId: string;
  isOpen: boolean;
  layer: number;
  onClickOverlay: (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  onExit?: (node: HTMLElement) => void;
}
export function defaultDrawerProps(): Partial<DrawerProps> {
  return {
    children: [],
    drawerId: 'drawer',
    onClickOverlay: () => {},
  };
}
