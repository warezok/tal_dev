import React from 'react';
import { shallow } from 'enzyme';
import { Header } from './Header';

describe('Header component test', () => {
  it("Header should have 'header' class", () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.hasClass('header')).toEqual(true);
    wrapper.unmount();
  });
});
