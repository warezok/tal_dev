/* eslint camelcase:off */
/* eslint no-shadow:off */

export enum Gender {
  FEMALE = 'Female',
  MALE = 'Male',
}
export interface BriefUser {
  firstName: string;
  id: string;
  lastName: string;
}
export interface DetailedUser {
  country: string;
  email: string;
  first_name: string;
  gender: Gender;
  id: string;
  job_title: string;
  last_name: string;
}

export class UserDataState {
  userDetails?: DetailedUser;
  userList: Array<BriefUser> = [];
}
