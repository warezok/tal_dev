import { UserDataState } from './UserDataState';

export interface AppState {
  UserData: UserDataState;
}
