/* eslint no-console: off */
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { AxiosResponse } from 'axios';
import { apiClient } from '../apiClient';
import { GET_USER, GET_USERS_LIST } from '../actions/UserDataActions';

function* getUsers() {
  try {
    const userListResponse: AxiosResponse = yield call(apiClient, 'users');
    yield put({
      type: GET_USERS_LIST.SUCCESS,
      userList: userListResponse.data,
    });
  } catch (error) {
    yield put({
      error,
      type: GET_USERS_LIST.FAILURE,
    });
  }
}

function* getUser(action: { id: string; type: typeof GET_USER.REQUEST }) {
  try {
    const { id } = action;
    const userDataResult: AxiosResponse = yield call(apiClient, `users/${id}`);
    yield put({
      type: GET_USER.SUCCESS,
      userData: userDataResult.data,
    });
  } catch (error) {
    yield put({
      error,
      type: GET_USER.FAILURE,
    });
  }
}

export function* watchUserData() {
  yield all([takeLatest(GET_USERS_LIST.REQUEST, getUsers), takeLatest(GET_USER.REQUEST, getUser)]);
}
