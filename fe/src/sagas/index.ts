import { all, fork } from 'redux-saga/effects';
import { watchUserData } from './UserDataSaga';

export function* saga() {
  yield all([fork(watchUserData)]);
}
