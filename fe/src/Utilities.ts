/* eslint no-console:off */
import { AppState } from './types/AppState';
import { APP_STATE_KEY } from './utilities/constants';

export const saveApplicationState = (state: Partial<AppState>) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(APP_STATE_KEY, serializedState);
  } catch (err) {
    console.log(err);
  }
};

export const loadApplicationState = (): AppState | undefined => {
  try {
    const serializedState = localStorage.getItem(APP_STATE_KEY);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};
