import { AppState } from '../types/AppState';

export const selectUsers = (state: AppState) => state.UserData.userList;
export const selectDetails = (state: AppState) => state.UserData.userDetails;
