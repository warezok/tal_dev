import React, { FC } from 'react';
import './App.scss';
import { Header } from './components/Header/Header';
import { Footer } from './components/Footer/Footer';
import { UserList } from './components/UserList/UserList';

export const App: FC = () => (
  <div className="App">
    <Header />
    <UserList />
    <Footer />
  </div>
);
