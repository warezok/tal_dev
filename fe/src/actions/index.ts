/* eslint no-redeclare:off */
import { Action, ActionWithPayload, ActionWithPayloads, CreateLifecycleTypes, CreateRequestTypes } from './types';

export function action<T extends string>(type: T): Action<T>;
export function action<T extends string, P extends {}>(type: T, payload: P): ActionWithPayloads<T, P>;
export function action<T extends string, P extends {} | undefined>(type: T, payload?: P) {
  return payload ? { type, ...payload } : { type };
}

export const actionWithPayload = <T extends string, P extends {} | undefined = undefined>(type: T, payload?: P): ActionWithPayload<T, P> =>
  (payload ? { payload, type } : { type }) as ActionWithPayload<T, P>;

export function createRequestTypes(type: string): CreateRequestTypes {
  return {
    FAILURE: `${type}_FAILURE`,
    REQUEST: `${type}_REQUEST`,
    SUCCESS: `${type}_SUCCESS`,
  };
}

export function createLifecycleTypes(type: string): CreateLifecycleTypes {
  return {
    MOUNT: `${type}_MOUNT`,
    UNMOUNT: `${type}_UNMOUNT`,
    UPDATE: `${type}_UPDATE`,
  };
}
