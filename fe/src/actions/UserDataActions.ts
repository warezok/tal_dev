import { action, createRequestTypes } from './index';

export const GET_USERS_LIST = createRequestTypes('GET_USERS_LIST');
export const GET_USER = createRequestTypes('GET_USER');
export const CLOSE_USER = 'CLOSE_USER';

export const getUsers = () => action(GET_USERS_LIST.REQUEST);
export const closeUser = () => action(CLOSE_USER);
export const getUser = (id: string) => action(GET_USER.REQUEST, { id });
