export interface Action<T extends string> {
  type: T;
}

export type ActionWithPayloads<T extends string, P extends {}> = Action<T> & P;

export type ActionWithPayload<T extends string, P extends {} | undefined = undefined> = P extends undefined
  ? { type: T }
  : { payload: P; type: T };

export interface CreateRequestTypes {
  FAILURE: string;
  REQUEST: string;
  SUCCESS: string;
}

export interface CreateLifecycleTypes {
  MOUNT: string;
  UNMOUNT: string;
  UPDATE: string;
}

export type DateString = string;

export type ActionCreatorType<T> = T extends (...args: infer A) => any ? (...args: A) => void : never;
