import { AnyAction } from 'redux';
import { UserDataState } from '../types/UserDataState';
import { CLOSE_USER, GET_USER, GET_USERS_LIST } from '../actions/UserDataActions';

export const UserData = (state = new UserDataState(), action: AnyAction) => {
  switch (action.type) {
    case GET_USERS_LIST.REQUEST:
      return { ...state, userList: [] };

    case GET_USERS_LIST.SUCCESS:
      return { ...state, userList: action.userList };

    case GET_USER.SUCCESS:
      return { ...state, userDetails: action.userData };
    case CLOSE_USER:
      return { ...state, userDetails: undefined };
    default:
      return state;
  }
};
