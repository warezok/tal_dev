import { combineReducers } from 'redux';
import { UserData } from './UserDataReducers';

const appReducer = () =>
  combineReducers({
    UserData,
  });

export default appReducer;
