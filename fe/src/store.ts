import { throttle } from 'lodash';
import { AnyAction, applyMiddleware, createStore } from 'redux';
import { createBrowserHistory } from 'history';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import { loadApplicationState, saveApplicationState } from './Utilities';
import appReducer from './reducers';
import { saga } from './sagas';
import { AppState } from './types/AppState';
import { APP_STATE_KEY } from './utilities/constants';

export const history = createBrowserHistory();
const sagaMonitor = require('@redux-saga/simple-saga-monitor');

const saveApplicationStateFrequency = 1000;

const rootReducer = (state: AppState | undefined, action: AnyAction) => appReducer()(state, action);

const persistedState = loadApplicationState();

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor });
  const composeEnhancers = composeWithDevTools({ trace: true });

  const storeCreator = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(routerMiddleware(history), sagaMiddleware))
  );

  sagaMiddleware.run(saga);

  window.addEventListener('storage', (event) => {
    if (event.key !== APP_STATE_KEY) {
      return;
    }
    if (!event.newValue && !window.location.href.includes('login') && !window.location.href.includes('invite')) {
      window.location.href = '/login';
      return;
    }

    const isOrgChanged =
      JSON.parse(event.newValue || '{}')?.UserData?.selectedOrganization?.organizationId !==
      JSON.parse(event.oldValue || '{}')?.UserData?.selectedOrganization?.organizationId;

    if (isOrgChanged) {
      if (window.location.href.includes('login')) return;

      window.location.href = '/library';
    }
  });

  return storeCreator;
}

export const store = configureStore();

export const saveState = () => {
  const state = store.getState();
  saveApplicationState({
    UserData: state.UserData,
  });
};

store.subscribe(throttle(saveState, saveApplicationStateFrequency));
